
//1
const paragraphs = document.querySelectorAll("p");

for (let item of paragraphs) {
  item.style.backgroundColor = '#ff0000'
}

//2
let opt = document.getElementById("optionsList");
console.log(opt);

let optParent = opt.parentNode
console.log(optParent);

let optChildren = opt.children;
console.log(optChildren);

let i = 0;
for (let elem of optChildren) {
  i++;
  console.log(i, elem.nodeName, elem.nodeType);
}

//3
let testP = document.getElementById("testParagraph");
testP.innerText = "This is a paragraph";

//4-5
let mainHeaderElements = document.querySelector(".main-header").children;
console.log(mainHeaderElements);

for (let elem of mainHeaderElements) {
  console.log(elem);
  elem.classList.add("nav-item");
}

//6
let sectionTitleElm = document.getElementsByClassName("section-title");
console.log(sectionTitleElm);
for (let elm of sectionTitleElm) {
  elm.classList.remove("section-title");
}
