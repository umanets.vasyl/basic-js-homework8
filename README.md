1. DOM - це подання документа у вигляді ієрархічної структури або дерева об'єктів, доступне для зміни через JavaScript

2. innerHTML повертає всі дочірні елементи, вкладені всередині тега (тег HTML + текстовий вміст). innerText повертає текстовий вміст дочірніх елементів, вкладених усередині мітки.

3. getElementById, getElementsByName, getElementsByClassName, getElementsByTagName, querySelector, querySelectorAll
Найпоширеніші - getElementById, querySelector, querySelectorAll